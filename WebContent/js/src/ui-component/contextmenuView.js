/**
 * @author zhaopeng
 * @datetime 2013-08-27
 * @description 在detailpanel页面加载完成后,加载公共组件
 */
define(function(require, exports, module){

	var $ = require('jquery');
	// Bootstrap 插件 
	var Bootstrap = require('bootstrap');
	var Contextmenu = require('contextmenu');
	var contextmenuTpl = '	<div data-toggle="contextmenu"> <ul class="dropdown-menu" role="menu"> </ul> </div>';

	var ContextmenuView = function(e,html,callback){
        if (3 == e.which) {//3:鼠标右键
			this.selecter = $(e.currentTarget);
			this.root = $(contextmenuTpl);
			this.contextmenuId = this.selecter.attr('data-contextmenu')==undefined?this.setContextmenuId():this.selecter.attr('data-contextmenu');
			this.html=html;
			this.callback=callback;
			this.init();              
        }		

	} 
	ContextmenuView.prototype.init = function() {
		this.root.find('.dropdown-menu').html(this.html);
		if(this.selecter.attr('data-contextmenu')==undefined){
			this.root.attr('id',this.contextmenuId);
			this.root.appendTo('body');			
		}
		this.open();
	};
	ContextmenuView.prototype.open = function() {

		this.selecter.attr('data-contextmenu',this.contextmenuId);
		var self = this;
		this.selecter.contextmenu({
            target: '#'+this.contextmenuId,
            onItem: function(e, item) {
            	self.callback($(item),self.selecter);
            }
        });  		
	};

	ContextmenuView.prototype.close = function() {
		
		
	};	
	ContextmenuView.prototype.getRoot = function() {
		return this.root;
	};

	ContextmenuView.prototype.setContextmenuId = function () {
	  var d = new Date();
	  var vYear = d.getFullYear();
	  var vMon = d.getMonth()+1;
	  var vDay = d.getDate();
	  var h = d.getHours();
	  var m = d.getMinutes();
	  var se = d.getSeconds();
	  var sse=d.getMilliseconds();
	  return 'contextmenu_'+vYear+vMon+vDay+h+m+se+sse;
	}	
	module.exports = ContextmenuView;
})
