/**
 * @author zhaopeng
 * @datetime 2013-08-27
 * @description 在detailpanel页面加载完成后,加载公共组件
 */
define(function(require, exports, module){

	var $ = require('jquery');
	// Bootstrap 插件 
	var Bootstrap = require('bootstrap');
	var Dropdownx = require('dropdownx');
	var dropdownTpl = '	<div class="dropdownx dropdownx-tip">  </div>';

	var DropdownView = function(selecter,html,callback){
		this.selecter = selecter;
		this.root = $(dropdownTpl);
		this.dropdownId = $(this.selecter).attr('data-dropdownx')==undefined?this.setDropdownId():$(this.selecter).attr('data-dropdownx');
		this.html=html;
		this.callback=callback;
		this.init();
	} 
	DropdownView.prototype.init = function() {
		this.root.html(this.html);
		if($(this.selecter).attr('data-dropdownx')==undefined){
			this.root.attr('id',this.dropdownId);
			this.root.appendTo('body');			
		}
		
	};
	DropdownView.prototype.open = function() {
		$(this.selecter).dropdownx('attach',this.dropdownId);
		$(this.selecter).dropdownx('show');		
	};

	DropdownView.prototype.close = function() {
		
		
	};	
	DropdownView.prototype.getRoot = function() {
		return this.root;
	};

	DropdownView.prototype.setDropdownId = function () {
	  var d = new Date();
	  var vYear = d.getFullYear();
	  var vMon = d.getMonth()+1;
	  var vDay = d.getDate();
	  var h = d.getHours();
	  var m = d.getMinutes();
	  var se = d.getSeconds();
	  var sse=d.getMilliseconds();
	  return 'dropdown_'+vYear+vMon+vDay+h+m+se+sse;
	}	
	module.exports = DropdownView;
})
