define(function(require, exports, module) {

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');
    // 右键插件
    var Contextmenu = require('contextmenu');


    var ContextmenuView = require('../../ui-component/contextmenuView');

    // 左侧边栏
    var sidebarTasklistHtml = require('./tpl/sidebar-tasklist.html');
    var tasklistContextmenuHtml = require('./tpl/tasklistContextmenu.html');

    var TasklistEditWidget = require('../../widget/tasklistEditWidget');

    var SidebarTasklist = Backbone.View.extend({
        el: '#sidebar-tasklist',
        events: {
            'click li': 'switchTasklist',
            'click .tasklist-add':'tasklistAdd',
            'mousedown #sidebar-tasklistlist>li':"tasklistContextmenu"
        },
        initialize: function() {
            Util.Models.get('tasklist').getTasklistList(
                Util.curProjectId,
                $.proxy(this.loadTasklistList, this)
            );
        },
        tasklistContextmenu:function(e){
            new ContextmenuView(e,tasklistContextmenuHtml,function(item,curTarget){
                var operate = item.attr('data-operate');
                if('edit'==operate){
                    new TasklistEditWidget();
                }else if ('add'==operate) {
                    new TasklistEditWidget();
                } else if('del'==operate){
                    curTarget.remove();
                };
            });
        },
        tasklistAdd:function(e){
            e.preventDefault();
            e.stopPropagation();
            new TasklistEditWidget();
        },
        loadTasklistList: function(tasklistList) {
            tasklistList.curProjectId=Util.curProjectId;
            this.$el.html(Util.tpl.t(sidebarTasklistHtml, tasklistList)).hide().slideToggle('slow');
            this.$el.trigger('updateCommon');
        },
        switchTasklist: function(e) {//切换tasklist
            var curTasklist = $(e.currentTarget).find('a');

            if (curTasklist.hasClass('active')) {
                return;
            } else {
                $('#detail-panel').css({
                    width: '0%'
                });                
                $('#list-panel').css({
                    width: '100%'
                });                
                this._clearActiveTasklist();
                curTasklist.addClass('active');
                Util.curTasklistType='tasklist';//设置当前选择的任务列表的类型是tasklist,我的任务是'myTasklist',用户的是'userTasklist',用来控制列表是显示我的任务,还是按照任务列表显示,或者显示某一个用户的任务
                Util.curTasklistId=curTasklist.attr('data-id');//设置当前选择的任务列表id
                this.$el.trigger('updateCommon');
            }
        },
        _clearActiveTasklist: function() {//清除激活的tasklist
            $('.sidebar-menu>ul>li').each(function(index, val) {
                var activeTasklist = $(this).find('a');
                if (activeTasklist.hasClass('active')) {
                    activeTasklist.removeClass('active');
                    return;
                }
            });
        }

    });
    module.exports = SidebarTasklist;
})