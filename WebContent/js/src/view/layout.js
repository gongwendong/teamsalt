/**
 * @author zhaopeng
 * @datetime 2013-08-27
 * @description 程序任务主界面入口
 */
define(function(require, exports, module){

	var $ 			= require('jquery');
	var _ 			= require('underscore');
	var Backbone 	= require('backbone');
	var Bootstrap 	= require('bootstrap');

	// 布局模板
	var layoutHtml 	= require('./tpl/layout.html');

	// 模板
	var ProjectBar 	= require('./header/projectBar');// header左侧项目切换bar
	var UserinfoBar = require('./header/userinfoBar');// header右侧用户信息bar
	var SidebarTasklist 	= require('./sidebar/sidebar-tasklist');// 左侧侧边栏
	var SidebarUser 	= require('./sidebar/sidebar-user');// 左侧侧边栏
	var ListPanel 	= require('./container/listPanel');// container左侧列表
	var DetailPanel = require('./container/detailPanel');// container右侧详情
	var CommonView = require('./commonView');// 公共组件VIEW

    //模型
    var Comment =  require('../model/comment');
    var Project =  require('../model/project');
    var Task =  require('../model/task');
    var Tasklist =  require('../model/tasklist');
    var User =  require('../model/user');

	var Laoyout = Backbone.View.extend({
		el: 'body',
        events:{
        	'updateCommon #sidebar':'updateCommon',
        	'updateCommon #list-panel':'updateCommon',
            'updateCommon #detail-panel':'updateCommon',
            'deleteTask #detail-panel':'deleteTask',
             'updateTask #detail-panel':'updateTask'
        },			
        initialize: function() {
            this.$el.html(Util.tpl.t(layoutHtml,{})).hide().show('1000');
            // 初始化模型,全部记载到map中
            this.comment  =  new Comment();
            this.project  =  new Project();
            this.task     =  new Task();
            this.tasklist =  new Tasklist();
            this.user     =  new User();

            Util.Models.put('comment',this.comment);
            Util.Models.put('project',this.project);
            Util.Models.put('task',this.task);
            Util.Models.put('tasklist',this.tasklist);
            Util.Models.put('user',this.user);
            
            // 初始化布局界面
            this.projectBar = new ProjectBar();
            this.userinfoBar = new UserinfoBar();
            this.sidebarTasklist = new SidebarTasklist();
            this.sidebarUser = new SidebarUser();
            this.listPanel = new ListPanel();
            this.detailPanel = new DetailPanel();

            Util.Views.put('projectBar',this.projectBar);
            Util.Views.put('userinfoBar',this.userinfoBar);
            Util.Views.put('sidebarTasklist',this.sidebarTasklist);
            Util.Views.put('sidebarUser',this.sidebarUser);
            Util.Views.put('listPanel',this.listPanel);
            Util.Views.put('detailPanel',this.detailPanel);
        },
        updateCommon:function(e){
        	CommonView.init();
        },
        //删除任务后,同步删除列表中的数据
        deleteTask:function(e,result){
        	this.listPanel.deleteTask(result.uuid);
        },
        //更新任务后,同步更新列表中的数据
        updateTask:function(e,result){
        	this.listPanel.updateTask(result);
        }
	});

	module.exports = Laoyout;
})
