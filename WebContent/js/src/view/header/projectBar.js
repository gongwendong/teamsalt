define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');
		
	// 详情模板
	var projectBarHtml = require('./tpl/project-bar.html');

    var ProjectEditWidget = require('../../widget/projectEditWidget');

	var ProjectBar = Backbone.View.extend({
		el: '#project-bar',
        events:{
            'click .project-add':'projectAdd'
        },
        initialize: function() {
        	var project = Util.Models.get('project');
        	project.getProjectList(
        		Util.curProjectId,
        		$.proxy(this.loadProjectList,this)
        	);
        },
        projectAdd:function(e){
            new ProjectEditWidget(e);
        },
        loadProjectList:function(projectList){
            console.log(projectList);
        	this.$el.html(Util.tpl.t(projectBarHtml, projectList));
        }
	});
	
	module.exports = ProjectBar;
})
