define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');
	
	
    var UserProfileWidget = require('../../widget/userProfileWidget');

	// 详情模板
	var userinfoBarHtml = require('./tpl/userinfo-bar.html');
	var UserinfoBar = Backbone.View.extend({
		el: '#userinfo-bar',
        initialize: function() {
        	var user = Util.Models.get('user');
        	user.getUser(
        			Util.curProjectId,
        		$.proxy(this.loadUserinfo,this),
        		$.proxy(this.errorLoadUserinfo,this)
        	);
            
        },
        events:{
            'click .profile-modal':'profile'
        },  
        profile:function(e){
        	new UserProfileWidget(e);
        },   
        loadUserinfo:function(userinfo){
        	this.$el.html(Util.tpl.t(userinfoBarHtml, userinfo));
        },
        errorLoadUserinfo:function(model, xhr, options){
        	console.log(model, xhr, options);
        }	        
	});
	
	module.exports = UserinfoBar;
})
