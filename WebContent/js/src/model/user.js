define(function(require, exports, module) {

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	var User = Backbone.Model.extend({
		getUser : function(params, success) {
			var url = '/user/profile/';
			var self = this;
			Util.query.g(url, '', function(result) {
				success(result.other, params);
			});
		},
		getUsersByProjectIdAndTaskId : function(params, success, error) {
			this.fetch({
				url : '/data/' + Util.curProjectId + '-users.json',
				success : function(model, response, options) {
					success(response);
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					error(arguments);
				}
			});
		},
		getUsersByProject : function(params, success, error) {
			var url = '/user/getProjectUsers';
			Util.query.g(url, params, function(result) {
				success(result.other);
			});
		},
		// 得到所有用户
		getAllUser : function(params, success) {
			var url = '/user/getAllUser';
			Util.query.g(url, '', function(result) {
				success(result.other);
			});
		}
	});
	module.exports = User;
})