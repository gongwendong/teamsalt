define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	var Tasklist = Backbone.Model.extend({
		getTasklistList: function(params, success, error) {
			var url = '/tasklist/getTasklistListByProject/'+params;
			Util.query.g(url,'',function(result){
				success(result.other);
			});
		}
	});
	module.exports = Tasklist;
})