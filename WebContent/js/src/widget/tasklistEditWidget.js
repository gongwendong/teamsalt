define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');


	//使用artTemplate模板引擎
	var ModalView = require('../ui-component/modalView');

    // 指定分配人模板
    var tasklistEditHtml = require('./tpl/tasklist-edit.html');

	var tasklistEditWidget = function(curTarget){
		this.curTarget = curTarget;
		this.init();
	}
	tasklistEditWidget.prototype.init = function() {
		this.modalView = new ModalView('任务列表',Util.tpl.t(tasklistEditHtml, {projectUuid:Util.curProjectId}));
        this.modalView.getRoot().find('.btn-primary').on('click',$.proxy(this.saveTasklist,this));
        this.modalView.open();
	};
	tasklistEditWidget.prototype.saveTasklist = function(e) {
		var self = this;
		Util.query.p('/tasklist/savef',this.modalView.getRoot().find('form').serialize(),function(result){
			Util.Views.get('sidebarTasklist').initialize();
			self.modalView.close();
			Util.notice.n(result.message);
		});
	
	};
	
	module.exports = tasklistEditWidget;
})