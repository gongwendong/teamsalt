define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');


	//使用artTemplate模板引擎
	var ModalView = require('../ui-component/modalView');

    // 指定分配人模板
    var userProfileHtml = require('./tpl/user-profile.html');

	var userProfileWidget = function(curTarget){
		this.curTarget = curTarget;
		this.init();
	}
	userProfileWidget.prototype.init = function() {
		this.ModalView = new ModalView('个人设置',Util.tpl.t(userProfileHtml, {}));
        this.ModalView.open();
	};
	userProfileWidget.prototype.selectUser = function(e) {
	
	};
	
	module.exports = userProfileWidget;
})