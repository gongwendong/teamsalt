define(function(require, exports, module) {

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	// 使用artTemplate模板引擎
	var ModalView = require('../ui-component/modalView');

	// 指定分配人模板
	var inviteUserHtml = require('./tpl/invite-user.html');

	var inviteUserWidget = function(curTarget,projectUuid) {
		this.curTarget = curTarget;
		this.projectUuid=projectUuid;
		this.init();
	}
	inviteUserWidget.prototype.init = function() {
		this.modalView = new ModalView('邀请用户', Util.tpl.t(inviteUserHtml, {projectUuid:this.projectUuid}));
		this.modalView.getRoot().find('.btn-primary').on('click',
				$.proxy(this.inviteUser, this));
		this.modalView.open();
	};
	// 用邮箱邀请用户加入
	inviteUserWidget.prototype.inviteUser = function(e) {
		e.preventDefault();
		e.stopPropagation();
		var self = this;
		Util.query.p('/project/invite', this.modalView.getRoot().find('form')
				.serialize(), function(result) {
			Util.Views.get('sidebarTasklist').initialize();
			self.modalView.close();
			console.log(result.message);
			Util.notice.n(result.message);
		});
	};

	module.exports = inviteUserWidget;
})