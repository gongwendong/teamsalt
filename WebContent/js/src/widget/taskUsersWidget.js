define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	//使用artTemplate模板引擎
	var DropdownView = require('../ui-component/dropdownView');

    // 指定分配人模板
    var taskUsersHtml = require('./tpl/task-users.html');


	var AaskUsersWidget = function(curTarget,taskId){
		this.curTarget = curTarget;
		this.init();
	}
	AaskUsersWidget.prototype.init = function() {
		var self = this;
		Util.Models.get('user').getUsersByProject({projectUuid:Util.curProjectId},function(result){
			self.userList = result;
			self.aaskPriorityDropdownView = new DropdownView(self.curTarget,Util.tpl.t(taskUsersHtml, self.userList));
	        self.aaskPriorityDropdownView.open();
	        self.aaskPriorityDropdownView.getRoot().find('li>a').on('click', $.proxy(self.selectUser, self));
		});			
	};
	AaskUsersWidget.prototype.selectUser = function(e) {
		var curUsername = $(e.currentTarget).text();
		var curUserId = $(e.currentTarget).attr('data-id');
		this.curTarget.val(curUsername)
		this.curTarget.prev().val(curUserId)
	};
	
	module.exports = AaskUsersWidget;
})