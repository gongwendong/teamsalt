define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');
	



	//使用artTemplate模板引擎
	var ModalView = require('../ui-component/modalView');

    // 指定分配人模板
    var projectEditHtml = require('./tpl/project-edit.html');

	var projectEditWidget = function(curTarget){
		this.curTarget = curTarget;
		this.init();
	};
	projectEditWidget.prototype.init = function() {
		this.modalView = new ModalView('列表',Util.tpl.t(projectEditHtml, {}));

        this.modalView.getRoot().find('.btn-primary').on('click',$.proxy(this.saveProject,this));
        this.modalView.open();	
	};
	projectEditWidget.prototype.saveProject = function(e) {
		var self = this;
		Util.query.p('/project/addf',this.modalView.getRoot().find('form').serialize(),function(result){
			Util.Views.get('projectBar').initialize();
			self.modalView.close();
			Util.notice.n(result.message);
		});
	};
	
	module.exports = projectEditWidget;
})