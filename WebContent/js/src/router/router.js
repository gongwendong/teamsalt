define(function(require, exports, module){
	var $ = require('jquery');
	var _ = require('underscore');	
	var Backbone = require('backbone');
	var Router = Backbone.Router.extend({
		routes: {
			''										                        :  'default',
			'main/:projectid'						            :  'main',
			'main/:projectid/:type-:dataid'   	:  'tasklist'
		},
		default: function () {
		},
		main: function (projectid) {
			Util.curProjectId=projectid;
			var Layout = require('../view/layout');
			new Layout();
		},
		tasklist:function(projectid,type,dataid){
			// var curProjectId=Contants.curProjectId;
			// if(_.isUndefined(curProjectId) ||_.isNull(curProjectId)
			// ||_.isEmpty(curProjectId) ){
			// Contants.curProjectId=projectid;
			// var Layout = require('../views/layout');
			// new Layout();
			// }
			var listPanel= Util.Views.get('listPanel');
			listPanel.getTaskList(projectid,type,dataid);
		},
		project: function (projectid) {
		}		
	});

	module.exports = Router;
})