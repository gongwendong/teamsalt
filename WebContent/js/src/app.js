define(function (require, exports) {
	var Backbone = require('backbone');

	//全局工具类
	Util = require('./common/util');
	var Router = require('./router/router');

	var router = new Router();

	Backbone.history.start();
});