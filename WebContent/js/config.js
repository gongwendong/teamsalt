seajs.config({
    debug:true,
    preload: ['seajs/seajs-text/1.0.3/seajs-text'],
    alias:{
      'jquery'        : 'jquery/jquery/1.10.1/jquery',
      'backbone'        : 'gallery/backbone/1.0.0/backbone',
      'bootbox'       : 'gallery/bootbox/4.0.0/bootbox',
      'bootstrap'       : 'gallery/bootstrap/3.0.0/bootstrap',
      'contextmenu'     : 'gallery/contextmenu/2.1/contextmenu',
      'datepicker'      : 'gallery/datepicker/2013.12.03/datepicker',
      'dropdownx'       : 'gallery/dropdownx/1.0.0/dropdownx',
      'notify'        : 'gallery/notify/1.0.0/notify',
      'perfect-scrollbar'   : 'gallery/perfect-scrollbar/0.4.3/perfect-scrollbar',
      'popbox'        : 'gallery/popbox/1.0.0/popbox',
      'select'        : 'gallery/select/1.1.3/select',
      'tags'          : 'gallery/tags/1.4.0/tags',
      'template'        : 'gallery/template/2.0.1/template',
      'template-extensions' : 'gallery/template/2.0.1/template-extensions',
      'underscore'      : 'gallery/underscore/1.4.4/underscore',
      'xdate'         : 'gallery/xdate/0.8/xdate'
    }
});