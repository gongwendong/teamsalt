package com.teamsalt.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.teamsalt.common.TeamsaltConstants;
/**
 * 
 * @author zhaopengCF
 * @time 2013-09-07 11:33:42
 * @description 使用Filter近过滤main.html页面,解决jfinal无法过滤静态html的问题.
 *
 */
public class MainFilter extends HttpServlet implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		if (null == session.getAttribute(TeamsaltConstants.LOGIN_USER)) {
			((HttpServletResponse) response).sendRedirect("/login");
			return;
		} else {
			filterChain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

}
