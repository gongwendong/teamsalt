package com.teamsalt.kit;

import com.jfinal.kit.StringKit;
import com.jfinal.plugin.activerecord.Model;
import com.teamsalt.filter.xss.HTMLFilter;

/**
 * 过滤特殊字符,防御xss
 */
public class HtmlTagKit {
	public static void htmlEncode(Model model) {
		String[] attrNames = model.getAttrNames();
		for (String attrName : attrNames) {
			String content = model.getStr(attrName);
			if (StringKit.notBlank(content)) {
				content = new HTMLFilter().filter(content);
				model.set(attrName, content);
			}
		}
	}
}
