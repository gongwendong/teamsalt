package com.teamsalt.common;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.render.ViewType;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.teamsalt.controller.CommentController;
import com.teamsalt.controller.IndexController;
import com.teamsalt.controller.InviteController;
import com.teamsalt.controller.ProjectController;
import com.teamsalt.controller.TaskController;
import com.teamsalt.controller.TasklistController;
import com.teamsalt.controller.UserController;
import com.teamsalt.interceptor.GlobalLoginInterceptor;
import com.teamsalt.model.Comment;
import com.teamsalt.model.Invite;
import com.teamsalt.model.InviteUser;
import com.teamsalt.model.Project;
import com.teamsalt.model.ProjectTasklist;
import com.teamsalt.model.ProjectUser;
import com.teamsalt.model.Task;
import com.teamsalt.model.Tasklist;
import com.teamsalt.model.TasklistTask;
import com.teamsalt.model.User;

/**
 * 
 * @ClassName: TeamsaltConfig
 * @Description: jfinal使用的配置文件
 * @author zhaopeng
 * @date 2013-6-21 下午2:54:00
 * 
 */
public class TeamsaltConfig extends JFinalConfig {

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		
		loadPropertyFile("config.txt");
		me.setDevMode(getPropertyToBoolean("devMode"));
		me.setViewType(ViewType.JSP);
		me.setError404View("/common/404.html");
		me.setError500View("/common/500.html");
	}

	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/", IndexController.class);
		me.add("/user", UserController.class);
		me.add("/project", ProjectController.class);
		me.add("/comment", CommentController.class);
		me.add("/task", TaskController.class);
		me.add("/tasklist", TasklistController.class);
		me.add("/invite", InviteController.class);
	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		String jdbcUrl = "";
		String user = "";
		String password = "";

		if (getPropertyToBoolean("baeEvn", false)) {// bae production
			jdbcUrl = getProperty("bae_jdbcUrl");
			user = getProperty("bae_user");
			password = getProperty("bae_password");
		} else {// location development
			jdbcUrl = getProperty("loc_jdbcUrl");
			user = getProperty("loc_user");
			password = getProperty("loc_password");
		}

		MysqlDataSource ds = new MysqlDataSource();
		ds.setUrl(jdbcUrl);
		ds.setUser(user);
		ds.setPassword(password);
		// 配置ActiveRecord插件
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin(ds);
		arp.setShowSql(true);
		me.add(arp);
		this.configModelMapping(arp);
		me.add(arp);
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());
		me.add(new GlobalLoginInterceptor());
	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
	}

	/**
	 * 初始化常量
	 */
	public void afterJFinalStart() {
	}

	/**
	 * 添加model与表的映射
	 * 
	 * @param arp
	 */
	private void configModelMapping(ActiveRecordPlugin arp) {
		arp.addMapping("user", "uuid", User.class);
		arp.addMapping("project", "uuid", Project.class);
		arp.addMapping("tasklist", "uuid", Tasklist.class);
		arp.addMapping("task", "uuid", Task.class);
		arp.addMapping("comment", "uuid", Comment.class);
		arp.addMapping("project_user", ProjectUser.class);
		arp.addMapping("project_tasklist", ProjectTasklist.class);
		arp.addMapping("tasklist_task", TasklistTask.class);
		arp.addMapping("invite", Invite.class);
		arp.addMapping("invite_user", InviteUser.class);
	}

}
