package com.teamsalt.common;

/**
 * 
 * @ClassName: T8Constants
 * @Description: T8中常量配置
 * @author zhaopeng
 * @date 2013-6-21 下午2:56:08
 * 
 */
public class TeamsaltConstants {
    // page size
    public static int TOPIC_PAGE_SIZE;
    public static int POST_PAGE_SIZE;
    public static int REPLY_PAGE_SIZE;
    public static int SIDEBAR_TOPIC_SIZE;
    public static int PAGE_SIZE_FOR_ADMIN;
    // others
    public static String ADMIN_EMAIL;
    public static String LOGIN_USER = "login_user";// 用户登录后,session存储的key

    // 动态墙常量
    public static class Trends {
        public static int type_create_project = 0;
        public static int type_create_topic = 1;
        public static int type_create_task = 2;
        public static int type_create_comments = 3;
    }
}
