package com.teamsalt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.teamsalt.common.Message;
import com.teamsalt.kit.ContextKit;
import com.teamsalt.kit.DateKit;
import com.teamsalt.kit.UuidKit;
import com.teamsalt.model.Invite;
import com.teamsalt.model.Project;
import com.teamsalt.model.ProjectUser;
import com.teamsalt.model.Task;
import com.teamsalt.model.User;

public class ProjectController extends Controller {
	private static final Logger log = Logger.getLogger(ProjectController.class);

	@Before(GET.class)
	public void index() {
		List list = Project.dao.getProjectsByCreateUuid(ContextKit
				.getCurrentUserUuid(getRequest()));
		renderJson(list);
	}

	/**
	 * 有自己创建的项目列表
	 */
	@Before(GET.class)
	public void getProjectsByMe() {
		List list = Project.dao.getProjectsByCreateUuid(ContextKit
				.getCurrentUserUuid(getRequest()));
		Map map = new HashMap();
		map.put("list", list);
		Message msg = new Message(0, "成功", map);
		renderJson(msg);
	}

	@Before(GET.class)
	public void add() {
		render("/project/add.html");
	}

	@Before(POST.class)
	public void addf() {
		Project model = getModel(Project.class);
		model.set("uuid", UuidKit.uuid());
		User user = ContextKit.getCurrentUser(getRequest());
		model.set("create_uuid", user.get("uuid"));
		model.set("create_time", DateKit.getNowDateTime());
		model.set("parent_uuid", "0");
		model.save();

		// 将创建者加入为管理员
		ProjectUser puser = new ProjectUser();
		puser.set("project_uuid", model.getStr("uuid"));
		puser.set("user_uuid", user.get("uuid"));
		puser.set("is_admin", 1);
		puser.save();

		Map map = new HashMap();
		map.put("project", model);
		Message msg = new Message(0, "添加成功", map);

		renderJson(msg);

	}

	@Before(GET.class)
	public void edit() {
		render("/project/add.html");
	}

	/**
	 * 
	 * @Title: detail
	 * @Description: 项目详情,包含项目成员
	 * @param 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	@Before(GET.class)
	public void detail() {
		String puuid = getPara(0);
		Project model = Project.dao.findById(puuid);
		setAttr("projects", model);
		List userList = User.dao.getUsersByProjectUuid(puuid);
		setAttr("userList", userList);

		render("/project/detail.html");
	}

	@Before(POST.class)
	public void editf() {
		render("/project/add.html");
	}

	@Before(GET.class)
	public void addUser() {
		setAttr("puuid", getPara(0));
		render("/project/addUser.html");
	}

	/**
	 * 
	 * @Title: addUserf
	 * @Description: 向项目中添加用户,首先检查邮箱是否已经在系统中存在,如果存在,则直接添加,如果不存在,则自动创建用户进行添加.
	 * @param 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	@Before(POST.class)
	public void addUserf() {
		String email = getPara("email");
		User user = User.dao.getUserByEmail(email);
		if (user == null) {// 创建用户
			User newUser = new User();
			newUser.set("uuid", UuidKit.uuid());
			newUser.set("username", email);
			newUser.set("email", email);
			newUser.set("password", "1");
			newUser.set("description", "");
			newUser.set("create_time", DateKit.getNowDateTime());
			newUser.save();
			user = newUser;
		}
		// 保存项目用户
		String puuid = getPara("puuid");
		ProjectUser puser = new ProjectUser();
		puser.set("project_uuid", puuid);
		puser.set("user_uuid", user.get("uuid"));
		puser.set("is_admin", 0);
		puser.save();

		Map map = new HashMap();
		map.put("user", user);
		Message msg = new Message(0, "添加成功", map);
		renderJson(msg);
	}

	/**
	 * 
	 * @Title: userList
	 * @Description: 用户列表
	 * @param 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public void userList() {
		List userList = User.dao.getUsersByProjectUuid(getPara(0));
		renderJson(userList);
	}

	/**
	 * 邀请用户
	 */
	public void invite() {
		String projectUuid = getPara("projectUuid");
		String inviterName = getPara("inviterName");
		String inviterEmail = getPara("inviterEmail");

		User user = User.dao.queryUserProjectByEmail(projectUuid, inviterEmail);
		Map map = new HashMap();
		Message msg = null;
		if (user == null) {
			String nowDateTime = DateKit.getNowDateTime();
			Invite invite = new Invite();
			invite.set(Invite.code, UuidKit.uuid());// 邀请码依旧使用UUID
			invite.set(Invite.inviterUuid, UuidKit.uuid());
			invite.set(Invite.inviteeEmail, inviterEmail);
			invite.set(Invite.inviteeName, inviterName);
			invite.set(Invite.inviteTime, nowDateTime);
			invite.set(Invite.inviteOvertime, DateKit.addDate(nowDateTime, 1));// 过期时间为1天
			invite.set(Invite.projectUuid, projectUuid);
			invite.set(Invite.isValid, Invite.isValid_valid);
			invite.save();
			map.put("invite", invite);
			msg = new Message(0, "发送邀请成功!", map);
		} else {
			map.put("user", user);
			msg = new Message(0, "用户已经添加到项目中了!", map);
		}

		renderJson(msg);
	}
}
