package com.teamsalt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.POST;
import com.teamsalt.common.Message;
import com.teamsalt.kit.ContextKit;
import com.teamsalt.kit.DateKit;
import com.teamsalt.kit.UuidKit;
import com.teamsalt.model.Comment;

public class CommentController extends Controller {
	private static final Logger log = Logger.getLogger(CommentController.class);

	public void index() {
		String projectUuid = getPara("projectUuid");
		String taskUuid = getPara("taskUuid");
		List comments = Comment.dao.queryCommentsByTaskUuid(projectUuid,
				taskUuid);
		Map map = new HashMap();
		map.put("comments", comments);
		Message msg = new Message(0, "成功", map);
		renderJson(msg);
	}

	@Before(POST.class)
	public void addf() {
		Comment comment = getModel(Comment.class);
		comment.set("uuid", UuidKit.uuid());
		comment.set("user_uuid", ContextKit.getCurrentUserUuid(getRequest()));
		comment.set("create_time", DateKit.getNowDateTime());
		comment.save();

		Map map = new HashMap();
		map.put("comments", comment);
		Message msg = new Message(0, "添加成功", map);

		renderJson(msg);
	}

}
