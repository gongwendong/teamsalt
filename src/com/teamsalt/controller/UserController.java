package com.teamsalt.controller;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.teamsalt.common.Message;
import com.teamsalt.kit.ContextKit;
import com.teamsalt.model.User;

/**
 * 
 * @ClassName: UserController
 * @Description: 用户信息查看,更新
 * @author zhaopeng
 * @date 2013-6-21 下午3:43:23
 * 
 */
public class UserController extends Controller {
	@Before(GET.class)
	public void index() {
		render("/user/user.html");
	}
	/**
	 * 当前登录用户信息
	 */
	@Before(GET.class)
	public void profile() {
		User user = ContextKit.getCurrentUser(getRequest());
		user.remove(User.password);
		Map map = new HashMap();
		map.put("user", user);
		Message msg = new Message(0, "success", map);
		renderJson(msg);
	}
	@Before(POST.class)
	public void editf() {
		User user = getModel(User.class);
		user.myUpdate();
		setCookie("msg", "更新成功", 30);
		setSessionAttr("user", user);
		redirect("/user");
	}

	/**
	 * 得到所用用户
	 */
	@Before(GET.class)
	public void getAllUser() {
		Map map = new HashMap();
		map.put("userList", User.dao.getAllUser());
		Message msg = new Message(0, "success", map);
		renderJson(msg);
	}

	/**
	 * 得到项目中的用户
	 */
	@Before(GET.class)
	public void getProjectUsers() {
		Map map = new HashMap();
		String pid = getPara("projectUuid");
		map.put("userList", User.dao.getProjectUsers(pid));
		Message msg = new Message(0, "success", map);
		renderJson(msg);
	}
}
