package com.teamsalt.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

public class Project extends Model<Project> {
	public static final Project dao = new Project();

	public static final String tableName = "project";

	public static final String uuid = "uuid";
	public static final String name = "name";
	public static final String parentUuid = "parent_uuid";
	public static final String description = "description";
	public static final String createUuid = "create_uuid";
	public static final String status = "status";
	public static final String createTime = "create_time";

	/**
	 * 得到用户创建所有项目列表
	 * 
	 * @param createUuid
	 * @return
	 */
	public List<Project> getProjectsByCreateUuid(String createUuid) {

		List list = dao.find("select t1.* from project t1,project_user t2 where t1.uuid=t2.project_uuid and t2.user_uuid=?",
				createUuid);
		if (list.size() == 0) {// 获取demo
			list = dao.find("select * from project where uuid='demo'");
		}
		return list;
	}

	/**
	 * 获取用户设置的关注的第一个项目
	 * 
	 * @param userUuid
	 * @return
	 */
	public Project getFirstProject(String userUuid) {
		Project p = dao.findFirst("select t1.* from project t1,project_user t2 where t1.uuid=t2.project_uuid and t2.user_uuid=?",
				userUuid);
		if (p == null) {// 获取demo
			p = dao.findFirst("select * from project where uuid='demo'");
		}
		return p;
	}

	public int saveProjectUser(String projectUuid, String userUuid,
			String isAdmin) {
		String sql = "insert into  project_user (project_uuid ,user_uuid ,is_admin) values (?,?,?)";
		return Db.update(sql, projectUuid, userUuid, isAdmin);
	}


}
