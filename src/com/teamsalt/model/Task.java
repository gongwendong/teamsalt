package com.teamsalt.model;

import java.util.List;

import jodd.util.StringUtil;

import com.jfinal.plugin.activerecord.Model;

public class Task extends Model<Task> {
	public static final Task dao = new Task();

	public static final String tableName = "task";

	public static final String uuid = "uuid";
	public static final String name = "name";
	public static final String content = "content";
	public static final String userUuid = "user_uuid";
	public static final String creatUuid = "creat_uuid";
	public static final String projectUuid = "project_uuid";
	public static final String createTime = "create_time";
	public static final String status = "status";// 0 未开始, 1 进行中 ,2 已完成

	public static final int status_noBegin = 0;
	public static final int status_doing = 1;
	public static final int status_end = 2;

	/**
	 * 获取用户的任务,按照type和时间倒序
	 * 
	 * @param projectUuid
	 * @param userUuid
	 * @return
	 */
	public List getMyTaskList(String projectUuid, String userUuid) {
		return dao
				.find("select t1.*,t2.username,t2.avatar from task t1,user t2 where t1.user_uuid=t2.uuid and  t1.project_uuid=? and t2.uuid=? order by t1.status,t1.create_time desc",
						projectUuid, userUuid);
	}

	/**
	 * 得到task详情
	 * 
	 * @param projectUuid
	 * @param tasklistUuid
	 * @param taskuuid
	 * @return
	 */
	public Task getDetail(String projectUuid, String tasklistUuid,
			String taskuuid) {
		if (StringUtil.isEmpty(tasklistUuid)) {
			// 无任务列表分配sql
			String sql = "SELECT t1.uuid, t1. name, t1.content, t1.create_time, t1.assign_date, t1. status, t1.user_uuid, t2.username, t1.project_uuid, t1.tasklist_uuid FROM task t1, user t2 WHERE t1.user_uuid = t2.uuid AND t1.uuid = ?";
			return dao.findFirst(sql, taskuuid);

		} else {
			String sql = "SELECT t1.uuid, t1. name, t1.content, t1.create_time, t1.assign_date, t1. STATUS, t1.user_uuid, t2.username, t1.project_uuid, t1.tasklist_uuid, t3. name AS tasklist_name FROM task t1, user t2, tasklist t3 WHERE t1.user_uuid = t2.uuid AND t1.tasklist_uuid = t3.uuid AND t3.uuid=? AND t1.uuid = ?";
			return dao.findFirst(sql, tasklistUuid, taskuuid);
		}
	}
	/**
	 * 根据tasklistUuid获得tasks
	 * @param projectUuid
	 * @param tasklistUuid
	 * @return
	 */
	public List getTasksByTasklistUuid(String projectUuid,String tasklistUuid) {
		return dao.find(
				"select t1.*,t2.username,t2.avatar from task t1,user t2 where t1.user_uuid=t2.uuid and  t1.project_uuid=? and t1.tasklist_uuid=? order by t1.status,t1.create_time desc",
				projectUuid, tasklistUuid);
	}

	public List getAllTasklistByTasklistUuid(String projectUuid,
			String userUuid, String tasklistUuid) {
		return dao.find(
				"select t1.*,t2.username,t2.avatar from task t1,user t2 where t1.user_uuid=t2.uuid and  t1.project_uuid=? and t1.tasklist_uuid=? order by t1.status,t1.create_time desc",
				projectUuid, tasklistUuid);
	}
	public List getTasklistByUserUuid(String projectUuid,
			String userUuid) {
		return dao.find(
				"select t1.*,t2.username,t2.avatar from task t1,user t2 where t1.user_uuid=t2.uuid and  t1.project_uuid=? and t1.user_uuid=? order by t1.status,t1.create_time desc",
				projectUuid, userUuid);
	}
	public boolean updateFinish(String todoUuid) {
		this.set("uuid", todoUuid);
		this.set("status", "1");
		return this.update();
	}

	public boolean updateUnfinish(String todoUuid) {
		this.set("uuid", todoUuid);
		this.set("status", "0");
		return this.update();
	}
}
